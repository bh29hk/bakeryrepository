// Let's register Template7 helper so we can pass json string in links
Template7.registerHelper('json_stringify', function (context) {
	return JSON.stringify(context);
});

// Initialize the bakery app
var myApp = new Framework7({
	animateNavBackIcon: true,
	// Enable templates auto precompilation
	precompileTemplates: true,
	// Enabled pages rendering using Template7
	template7Pages: true
	// Specify Template7 data for pages
});

// Export selectors engine
var $$ = Dom7;
// load in the recipes JSON file and add to cakelist dataset
function getRecipes() {
	$$.getJSON('recipes.json', function (json) {
		myApp.template7Data.cakelist = json ;
	});
};

getRecipes();

// Add main View
var mainView = myApp.addView('.view-main', {
	// Enable dynamic Navbar
	dynamicNavbar: true,
});